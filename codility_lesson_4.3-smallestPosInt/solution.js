/* This is a demo task.

Write a function:

class Solution { public int solution(int[] A); }

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000]. */

setTimeout(() => {
    const A = [0,3,5,6,8,100,150, 1000000, -1000000]
    const res = solution(A)
    console.log(res)
}, 5*1000);


function solution(A) {

    const fillMap = {}
    let min = null
    let max = null

    for (let i = 0; i < A.length; i++) {
        const val = A[i];
        if(val>0){
            fillMap[val] = true
        }
        if(min == null || val < min){
            if(val > 0){
                min = val
            }
        }
        if(max == null || val > max){
            if(val>0){
                max = val
            }
        }
    }

    if(A.length == 1){
        if(A[0] == 1){
            return 2
        }else{
            return 1
        }
    }

    if(min == null && max == null){
        return 1;
    }
    for (let i = 1; i <= max; i++) {
        if(!fillMap[i]){
            return i;
        }
    }
    return max + 1;
}

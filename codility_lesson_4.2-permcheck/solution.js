/* A non-empty array A consisting of N integers is given.

A permutation is a sequence containing each element from 1 to N once, and only once.

For example, array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
is a permutation, but array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
is not a permutation, because value 2 is missing.

The goal is to check whether array A is a permutation.

Write a function:

class Solution { public int solution(int[] A); }

that, given an array A, returns 1 if array A is a permutation and 0 if it is not.

For example, given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
the function should return 1.

Given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
the function should return 0.

Write an efficient algorithm for the following assumptions:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [1..1,000,000,000]. */
setTimeout(() => {
    const A = [4,1,3]
    const res = solution(A)
    console.log(res)
}, 5*1000);


function solution(A) {

    let fillMap = {}
    const orderedArr = [];
    for (let i = 0; i < A.length; i++) {
        const curr = A[i];
        fillMap[curr] = true;
        orderedArr[i]= i+1;
    }

    const keys = Object.keys(fillMap);

    if(keys.length != A.length){
        return 0
    }else{
        for (let i = 0; i < orderedArr.length; i++) {
            const val = orderedArr[i]
            if(!fillMap[val]){
                return 0
            }
        }
    }

    return 1;
}
